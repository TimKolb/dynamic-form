import * as React from 'react';
import { lazy, Suspense } from 'react';
import { RouteComponentProps } from '@reach/router';
import { Spinner } from '../components/Spinner';

const Component: React.FC<RouteComponentProps> = lazy(() =>
  import('./404Page'),
);

export const Lazy404Page: React.FC<RouteComponentProps> = props => (
  <Suspense fallback={<Spinner />}>
    <Component {...props} />
  </Suspense>
);
