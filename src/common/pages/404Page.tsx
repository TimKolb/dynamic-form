import * as React from 'react';
import { Link, RouteComponentProps } from '@reach/router';
import { BaseLayout } from '../components/BaseLayout';
import { Col, Row } from 'antd';
import styled from '@emotion/styled';
import { NamespacesConsumer } from 'react-i18next';
import { Spinner } from '../components/Spinner';
import { Suspense } from 'react';
import { lazy } from 'react';

const NotFoundHeading = styled.h1`
  font-weight: bolder;
  font-size: 3rem;
  margin: 1rem;
`;

const NotFoundText = styled.p`
  font-weight: bold;
  margin: 1rem;
`;

const EmptyText = styled.p`
  font-weight: bolder;
  font-size: 1.45rem;
  color: #000;
`;

const CenterAll = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
`;

const EmptyDiv = styled.div`
  width: 50%;
  padding-top: 50%;
  border: 2px solid #000;
`;

const LazyGame: React.FC = lazy(() => import('../components/Game/Game'));

const NotFoundPage: React.FC<RouteComponentProps> = () => (
  <BaseLayout>
    <NamespacesConsumer>
      {t => (
        <Row type="flex">
          <Col xs={24} md={12}>
            <CenterAll>
              <NotFoundHeading>404</NotFoundHeading>
              <NotFoundText>{t('Oops! Well, you found nothing.')}</NotFoundText>
              <Link to="/">{t('Go Back')}</Link>
            </CenterAll>
          </Col>
          <Col xs={24} md={12}>
            <CenterAll>
              <EmptyDiv />
              <EmptyText>{t('Nothing')}</EmptyText>
            </CenterAll>
          </Col>
        </Row>
      )}
    </NamespacesConsumer>
    <CenterAll>
      <Suspense fallback={<Spinner />}>
        <LazyGame />
      </Suspense>
    </CenterAll>
  </BaseLayout>
);

export default NotFoundPage;
