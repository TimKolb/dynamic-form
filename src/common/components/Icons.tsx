import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faTrash,
  faPlus,
  faChevronUp,
  faChevronDown,
  faBars,
  faCog,
} from '@fortawesome/free-solid-svg-icons';

[faTrash, faPlus, faChevronUp, faChevronDown, faBars, faCog].forEach(icon =>
  library.add(icon),
);
