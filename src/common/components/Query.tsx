import React, { Component, createContext, ReactNode } from 'react';

const initialState: QueryState = {
  data: null,
  loading: false,
  error: null,
  abortController: undefined,
};

const initialQueryContextValue = {
  state: initialState,
  actions: {
    fetch: async () => {},
  },
};

type QueryContextType = {
  state: QueryState;
  actions: { fetch: (optionsPart?: Partial<QueryOptions>) => Promise<void> };
};

const QueryContext = createContext<QueryContextType>(initialQueryContextValue);

type QueryState = {
  data: any;
  loading: boolean;
  error: Error | null;
  abortController?: AbortController;
};

type QueryProps = {
  fetch: GlobalFetch['fetch'];
  disableInitialFetch: boolean;
  stateReducer: StateReducer;
  deserialize: (res: Response) => Promise<any>;
  url: string;
  options: QueryOptions;
  children: (context: QueryContextType) => ReactNode;
};

type StateReducer = (
  update: Partial<QueryState> | QueryState | null,
  state: Readonly<QueryState>,
  props: Readonly<QueryProps>,
) => Partial<QueryState> | QueryState | null;

type QueryOptions = RequestInit & {
  url?: string;
};

export class Query extends Component<QueryProps, QueryState> {
  static Consumer = QueryContext.Consumer;
  static defaultProps: Partial<QueryProps> = {
    fetch,
    disableInitialFetch: false,
    stateReducer: update => update,
    deserialize: async res => res.json(),
  };

  state: QueryState = initialState;
  isFetching: boolean = false;
  mounted: boolean = false;

  setReducedState: (
    update: Partial<QueryState> | QueryState | null,
  ) => void = update => {
    if (!this.mounted) {
      return;
    }
    const { stateReducer } = this.props;
    this.setState(state => stateReducer(update, state, this.props));
  };

  getAbortController = () => {
    let abortController, signal;
    if (typeof window !== 'undefined' && 'AbortController' in window) {
      abortController = new AbortController();
      signal = abortController.signal;
    }

    return { abortController, signal };
  };

  request = async (optionsPart?: Partial<QueryOptions>) => {
    this.isFetching = true;
    const { fetch, url: propUrl, options, deserialize } = this.props;

    const { abortController, signal } = this.getAbortController();

    this.setReducedState({ loading: true, abortController });

    // use the url from the request argument or fallback to the url from props
    let url = (optionsPart && optionsPart.url) || propUrl;
    let fetchOptions = options;
    if (optionsPart) {
      // strip the url key from the fetch options if it is provided
      const { url, ...restOptions } = optionsPart;
      fetchOptions = { signal, ...options, ...restOptions };
    }

    try {
      const res = await fetch(url, fetchOptions);
      const data = await deserialize(res);
      this.setReducedState({
        data,
        loading: false,
        error: null,
        abortController: undefined,
      });
    } catch (error) {
      this.setReducedState({
        loading: false,
        error,
        abortController: undefined,
      });
    }

    this.isFetching = false;
  };

  actions = {
    fetch: this.request,
  };

  async componentDidMount() {
    this.mounted = true;
    if (!this.isFetching && !this.props.disableInitialFetch) {
      await this.request();
    }
  }

  componentWillUnmount() {
    const { abortController } = this.state;
    if (abortController) {
      abortController.abort();
    }
    this.mounted = false;
  }

  render() {
    const { children } = this.props;

    const value = {
      state: this.state,
      actions: this.actions,
    };

    return (
      <QueryContext.Provider value={value}>
        {typeof children === 'function' ? children(value) : children}
      </QueryContext.Provider>
    );
  }
}
