import * as React from 'react';
import { Component } from 'react';
import { Alert, Button } from 'antd';

export class ErrorBoundary extends Component {
  state: { error: Error | null } = { error: null };

  static getDerivedStateFromError(error: Error) {
    return { error };
  }

  handleReset = () => {
    this.setState({ error: null });
  };

  render() {
    const { children } = this.props;
    const { error } = this.state;

    if (error) {
      return (
        <div>
          <Alert message={error.message} type="error" />
          <Button href="#" onClick={this.handleReset}>
            Retry
          </Button>
        </div>
      );
    }

    return children;
  }
}
