import { Layout, Layout as AntdLayout, Menu } from 'antd';
import * as React from 'react';
const { Content } = AntdLayout;
import { ReactNode } from 'react';
import styled from '@emotion/styled';
import { Link } from '@reach/router';

const { Header } = Layout;

const MainLayout = styled(AntdLayout)`
  min-height: 100%;
`;

const MainContent = styled(Content)`
  height: 100%;
  width: 100%;
  margin: 0 auto;
`;

const InnerContent = styled('div')`
  padding: 1.5rem;
  min-height: 100%;
`;

const InnerHeader = styled.div`
  display: flex;
  justify-content: space-between;
`;

const LogoTitle = styled.div`
  color: #fff;
  font-size: 1.85rem;
  font-weight: 300;
`;

type BaseLayoutProps = Partial<{
  menu?: ReactNode;
  logo?: ReactNode;
}>;

const AppHeader: React.FC = ({ children }) => (
  <Header>
    <InnerHeader>{children}</InnerHeader>
  </Header>
);

export const BaseLayout: React.FC<BaseLayoutProps> = ({
  logo,
  menu,
  children,
}) => (
  <MainLayout>
    <AntdLayout>
      <AppHeader>
        {logo}
        {menu}
      </AppHeader>

      <MainContent>
        <InnerContent>{children}</InnerContent>
      </MainContent>
    </AntdLayout>
  </MainLayout>
);

BaseLayout.defaultProps = {
  logo: (
    <Link to="/">
      <LogoTitle role="img">Teamly</LogoTitle>
    </Link>
  ),
};
