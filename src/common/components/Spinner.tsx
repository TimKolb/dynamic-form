import * as React from 'react';
import { CircleLoader, ReactSpinners } from 'react-spinners';
import { css } from 'emotion';

const loaderClass = css`
  display: block;
  margin: 1rem auto;
`;

export const Spinner: React.FC<ReactSpinners.CircleLoaderProps> = props => (
  <CircleLoader className={loaderClass} loading {...props} />
);
