import * as Yup from 'yup';
import * as React from 'react';
import { memo } from 'react';
import { Field, FieldProps, Formik, FormikProps } from 'formik';
import { Alert, Button, Form, Input } from 'antd';
import { NamespacesConsumer } from 'react-i18next';
import { getFormItemProps } from '../../dynamicQuestionnaire';
import { Query } from '../../../common/components/Query';
import { Spinner } from '../../../common/components/Spinner';

const FormItem = Form.Item;

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Enter a valid email address')
    .required('Required'),
  password: Yup.string().required('Required'),
});

type LoginValues = {
  email: string;
  password: string;
};

const initialLoginValues = {
  email: '',
  password: '',
};

export const Login: React.FC = memo(() => (
  <section>
    <Query url="/api/login" disableInitialFetch options={{ method: 'POST' }}>
      {({ state: { data, loading, error }, actions: { fetch } }) => {
        const errorComponent = error ? (
          <Alert type="error" message={error.message} />
        ) : null;

        return (
          <Formik
            validationSchema={loginSchema}
            initialValues={initialLoginValues}
            onSubmit={v => {
              fetch({ body: JSON.stringify(v) });
            }}
            render={(formikProps: FormikProps<LoginValues>) => {
              const { errors, touched, handleSubmit } = formikProps;
              return (
                <form onSubmit={handleSubmit}>
                  {errorComponent}
                  <NamespacesConsumer>
                    {t => (
                      <>
                        <FormItem
                          label={t('Email')}
                          {...getFormItemProps({
                            errors,
                            touched,
                            name: 'email',
                          })}
                        >
                          <Field
                            name="email"
                            render={({ field }: FieldProps) => (
                              <Input {...field} />
                            )}
                          />
                        </FormItem>

                        <FormItem
                          label={t('Password')}
                          {...getFormItemProps({
                            errors,
                            touched,
                            name: 'password',
                          })}
                        >
                          <Field
                            name="password"
                            render={({ field }: FieldProps) => (
                              <Input type="password" {...field} />
                            )}
                          />
                        </FormItem>
                      </>
                    )}
                  </NamespacesConsumer>

                  <Button htmlType="submit" type="primary">
                    <NamespacesConsumer>{t => t('Login')}</NamespacesConsumer>
                  </Button>
                </form>
              );
            }}
          />
        );
      }}
    </Query>
  </section>
));
