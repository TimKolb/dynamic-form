import * as React from 'react';
import { RouteComponentProps } from '@reach/router';
import { BaseLayout } from '../../../common/components/BaseLayout';
import { Login } from '../components';

const LoginPage: React.FC<RouteComponentProps> = () => (
  <BaseLayout>
    <Login />
  </BaseLayout>
);

export default LoginPage;
