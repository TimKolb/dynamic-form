import * as React from 'react';
import { lazy, Suspense } from 'react';
import { RouteComponentProps } from '@reach/router';
import { Spinner } from '../../../common/components/Spinner';

const Component: React.FC<RouteComponentProps> = lazy(() => import('./LoginPage'));

export const LazyLoginPage: React.FC<RouteComponentProps> = props => (
  <Suspense fallback={<Spinner />}>
    <Component {...props} />
  </Suspense>
);
