import { FormikProps, getIn } from 'formik';
import memoizeOne from 'memoize-one';
import { Question, Questionnaire } from './components/Edit';
import { FieldType, FieldTypeType } from './fieldTypes';

interface ValidationInfo {
  errors: FormikProps<any>['errors'];
  touched: FormikProps<any>['touched'];
  name: string;
}

export function getValidateStatus({
  errors,
  touched,
  name,
}: ValidationInfo): 'error' | undefined {
  return getIn(errors, name) && getIn(touched, name) ? 'error' : undefined;
}

export const getFormItemProps = memoizeOne(
  ({ errors, touched, name }: ValidationInfo) => {
    const validateStatus = getValidateStatus({ errors, touched, name });
    if (!validateStatus) {
      return {};
    }

    return {
      help: getIn(errors, name),
      validateStatus,
    };
  },
);

export const getDefaultQuestion: () => Question = () => ({
  id: Date.now().toString(),
  fieldType: FieldTypeType.text,
  question: '',
  rules: [],
});

export const buildInitialValues = memoizeOne(
  (questionnaire: Questionnaire, fieldTypes: FieldType[]) =>
    questionnaire.sections.map(s => ({
      name: '',
      questions: s.questions.reduce((acc, { fieldType, id }) => {
        const type = fieldTypes.find(f => f.type === fieldType);
        if (!type) {
          throw new Error(`No type found for ${fieldType}`);
        }
        return { ...acc, [id]: type.initialValue };
      }, {}),
    })),
);
