import * as React from 'react';
import { Input, Rate, Switch } from 'antd';
import * as Yup from 'yup';
import { BooleanSchema, NumberSchema, StringSchema } from 'yup';
import { FieldProps } from 'formik';
import { Rule } from './validationRules';
import styled from '@emotion/styled';
import { NamespacesConsumer } from 'react-i18next';

const { TextArea } = Input;

export enum FieldTypeType {
  shortText = 'shortText',
  text = 'text',
  rating = 'rating',
  yesNo = 'yesNo',
}

export type FieldType<S = any> = {
  type: FieldTypeType;
  name: string;
  Field: React.FC<CustomFieldProps>;
  Result: React.FC<{ value: any }>;
  yupType: any;
  ruleTypes: Rule<S>[];
  initialValue: any;
};

export type CustomFieldProps<Values = any> = FieldProps<Values> & {
  [key: string]: any;
};

export const fieldTypes = [
  {
    type: FieldTypeType.shortText,
    name: 'ShortText',
    Field: ({ field }: CustomFieldProps) => <Input {...field} />,
    Result: ({ value }) => <p>{value}</p>,
    yupType: Yup.string(),
    ruleTypes: [
      {
        type: 'minLength',
        label: 'Minimum length of two characters',
        add: yupSchema => yupSchema.min(2, 'Text is too short'),
      },
      {
        type: 'maxLength',
        label: 'Maximum length of 100 characters',
        add: yupSchema => yupSchema.max(100, 'Text is too long'),
      },
    ],
    initialValue: '',
  } as FieldType<StringSchema>,
  {
    type: FieldTypeType.text,
    name: 'Text',
    Field: ({ field }: CustomFieldProps) => <TextArea {...field} autosize />,
    Result: styled(({ value, ...restProps }) => <p {...restProps}>{value}</p>)`
      white-space: pre;
    `,
    yupType: Yup.string(),
    ruleTypes: [
      {
        type: 'minLength',
        label: 'Minimum length of five characters',
        add: yupSchema => yupSchema.min(5, 'Text is too short'),
      },
      {
        type: 'maxLength',
        label: 'Maximum length of 1000 characters',
        add: yupSchema => yupSchema.max(1000, 'Text is too long'),
      },
    ],
    initialValue: '',
  } as FieldType<StringSchema>,
  {
    type: FieldTypeType.rating,
    name: 'Rating',
    Field: ({ field: { onBlur, ...restField }, form }: CustomFieldProps) => (
      <Rate
        {...restField}
        count={10}
        onChange={value => form.setFieldValue(restField.name, value)}
      />
    ),
    Result: ({ value }) => <Rate count={10} disabled value={value} />,
    yupType: Yup.number(),
    ruleTypes: [
      {
        type: 'minOneStar',
        label: 'Minimum of one star',
        add: yupSchema =>
          yupSchema.min(
            1,
            'Please acknowledge question by selecting one star.',
          ),
      },
    ],
    initialValue: 0,
  } as FieldType<NumberSchema>,
  {
    type: FieldTypeType.yesNo,
    name: 'YesNo',
    Field: ({ field: { onBlur, ...restField }, form }: CustomFieldProps) => (
      <>
        <Switch
          {...restField}
          onChange={value => form.setFieldValue(restField.name, value)}
        />
        <span style={{ marginLeft: '1em' }}>
          <NamespacesConsumer>
            {t => t(restField.value ? 'Yes' : 'No')}
          </NamespacesConsumer>
        </span>
      </>
    ),
    Result: ({ value }) => <Switch disabled checked={value} />,
    yupType: Yup.number(),
    initialValue: false,
    ruleTypes: [],
  } as FieldType<BooleanSchema>,
];
