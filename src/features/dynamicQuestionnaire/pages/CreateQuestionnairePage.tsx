import * as React from 'react';
import { memo } from 'react';
import { QuestionnaireCreator } from '../components';
import { BaseLayout } from '../../../common/components/BaseLayout';
import { Menu } from 'antd';
import { ChangeLanguage } from '../../../ChangeLanguage';
import { RouteComponentProps } from '@reach/router';

const { SubMenu } = Menu;

const AppMenu = memo(() => (
  <ChangeLanguage>
    {({ language, languageIcons, onChangeLanguage }) => (
      <Menu
        theme="dark"
        mode="horizontal"
        style={{ lineHeight: '64px' }}
        onSelect={({ key }) => {
          onChangeLanguage(key);
        }}
        selectedKeys={[language]}
      >
        <SubMenu key="language" title={<span>Language</span>}>
          {Object.keys(languageIcons).map(key => (
            <Menu.Item key={key}>{languageIcons[key]}</Menu.Item>
          ))}
        </SubMenu>
      </Menu>
    )}
  </ChangeLanguage>
));

const CreateQuestionnairePage: React.FC<RouteComponentProps> = () => (
  <BaseLayout menu={<AppMenu />}>
    <QuestionnaireCreator />
  </BaseLayout>
);

export default CreateQuestionnairePage;
