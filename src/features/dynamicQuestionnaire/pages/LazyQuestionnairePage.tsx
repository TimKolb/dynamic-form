import * as React from 'react';
import { lazy, Suspense } from 'react';
import { RouteComponentProps } from '@reach/router';
import { Spinner } from '../../../common/components/Spinner';

const LazyQuestionnairePageComponent: React.FC<RouteComponentProps> = lazy(() =>
  import('./CreateQuestionnairePage'),
);

export const LazyQuestionnairePage: React.FC<RouteComponentProps> = props => (
  <Suspense fallback={<Spinner />}>
    <LazyQuestionnairePageComponent {...props} />
  </Suspense>
);
