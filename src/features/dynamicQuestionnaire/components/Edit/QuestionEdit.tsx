import * as React from 'react';
import { memo } from 'react';
import {
  ArrayHelpers,
  Field,
  FieldProps,
  FormikProps,
  FormikValues,
  getIn,
} from 'formik';
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from 'react-beautiful-dnd';
import { Card, Form as AntdForm, Input, Select } from 'antd';
import { ButtonBar } from './ButtonBar';
import { getFormItemProps } from '../../formHelpers';
import { FieldType, fieldTypes } from '../../fieldTypes';
import { genericRuleTypes } from '../../validationRules';
import { Question } from './Edit';
import { FormItemProps } from 'antd/lib/form';
import { NamespacesConsumer } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from '@emotion/styled';

const FormItem = AntdForm.Item;
const { TextArea } = Input;
const { Option } = Select;

const Extra = styled.div`
  display: flex;
`;

const GrabToMove = styled.div`
  display: flex;
  align-items: center;
  margin-left: 1rem;
`;

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
  userSelect: 'none',
  margin: '1rem 0',
  border: isDragging ? '1px dashed #CCC' : 'none',
  ...draggableStyle,
});

const QuestionTextEdit: React.FC<{
  formItemProps: Partial<FormItemProps>;
  fieldPath: string;
}> = memo(({ formItemProps, fieldPath }) => (
  <FormItem {...formItemProps} style={{ marginBottom: 0, marginRight: '1rem' }}>
    <Field
      name={`${fieldPath}.question`}
      render={({ field }: FieldProps) => (
        <NamespacesConsumer>
          {t => (
            <TextArea
              {...field}
              placeholder={t('Question')}
              autosize
              style={{ borderColor: 'transparent', fontSize: '1.2rem' }}
            />
          )}
        </NamespacesConsumer>
      )}
    />
  </FormItem>
));

const QuestionTypeEdit: React.FC<{
  formItemProps: Partial<FormItemProps>;
  fieldPath: string;
}> = memo(({ formItemProps, fieldPath }) => (
  <NamespacesConsumer>
    {t => (
      <FormItem label={t('Question type')} {...formItemProps}>
        <Field
          name={`${fieldPath}.fieldType`}
          render={({ field, form }: FieldProps) => (
            <Select
              {...field}
              onChange={v => {
                form.setFieldValue(`${fieldPath}.rules`, []);
                form.setFieldValue(field.name, v);
              }}
              showSearch
              placeholder={t('Select a type')}
              optionFilterProp="children"
              filterOption={(input, option) =>
                `${option.props.children}`
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
            >
              {fieldTypes.map(fieldType => (
                <Option key={fieldType.type} value={fieldType.type}>
                  {fieldType.name}
                </Option>
              ))}
            </Select>
          )}
        />
      </FormItem>
    )}
  </NamespacesConsumer>
));

const QuestionValidationRulesEdit: React.FC<{
  formItemProps: Partial<FormItemProps>;
  fieldPath: string;
  values: FormikValues;
}> = memo(({ formItemProps, values, fieldPath }) => (
  <NamespacesConsumer>
    {t => (
      <FormItem label={t('Validation rules')} {...formItemProps}>
        <Field
          name={`${fieldPath}.rules`}
          render={({ field: { onBlur, ...restField }, form }: FieldProps) => (
            <Select
              {...restField}
              onChange={v => form.setFieldValue(restField.name, v)}
              mode="multiple"
              placeholder={t('Add validation rules')}
              optionFilterProp="children"
              filterOption={(input, option) =>
                `${option.props.children}`
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
            >
              {[
                ...(() => {
                  const selectedFieldType = fieldTypes.find(
                    f => f.type === getIn(values, `${fieldPath}.fieldType`),
                  );

                  return (
                    (selectedFieldType && selectedFieldType.ruleTypes) || []
                  );
                })(),
                ...genericRuleTypes,
              ].map(({ type, label }) => (
                <Option key={type} value={type}>
                  {label}
                </Option>
              ))}
            </Select>
          )}
        />
      </FormItem>
    )}
  </NamespacesConsumer>
));

export const QuestionEdit: React.FC<{
  question: Question;
  questionIndex: number;
  all: Question[];
  arrayHelpers: ArrayHelpers;
  formikProps: FormikProps<any>;
  fieldTypes: FieldType[];
  fieldPath: string;
}> = memo(
  ({ question, questionIndex, all, arrayHelpers, formikProps, fieldPath }) => (
    <Draggable draggableId={question.id} index={questionIndex}>
      {(provided, snapshot) => (
        <InnerQuestionEdit
          all={all}
          arrayHelpers={arrayHelpers}
          formikProps={formikProps}
          fieldPath={fieldPath}
          provided={provided}
          snapshot={snapshot}
          question={question}
          questionIndex={questionIndex}
        />
      )}
    </Draggable>
  ),
);

const InnerQuestionEdit: React.FC<{
  all: Question[];
  arrayHelpers: ArrayHelpers;
  formikProps: FormikProps<any>;
  fieldPath: string;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  question: Question;
  questionIndex: number;
}> = memo(
  ({
    all,
    arrayHelpers,
    formikProps: { errors, touched, values },
    fieldPath,
    provided,
    snapshot,
    question,
    questionIndex,
  }) => (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
    >
      <Card
        id={question.id}
        title={
          <QuestionTextEdit
            formItemProps={getFormItemProps({
              errors,
              touched,
              name: `${fieldPath}.questions.${questionIndex}.question`,
            })}
            fieldPath={`${fieldPath}.questions.${questionIndex}`}
          />
        }
        extra={
          <Extra>
            <ButtonBar arrayHelpers={arrayHelpers} index={questionIndex} />
            <GrabToMove {...provided.dragHandleProps}>
              <FontAwesomeIcon icon="bars" />
            </GrabToMove>
          </Extra>
        }
      >
        <QuestionTypeEdit
          formItemProps={getFormItemProps({
            errors,
            touched,
            name: `${fieldPath}.questions.${questionIndex}.fieldType`,
          })}
          fieldPath={`${fieldPath}.questions.${questionIndex}`}
        />

        <QuestionValidationRulesEdit
          formItemProps={getFormItemProps({
            errors,
            touched,
            name: `${fieldPath}.questions.${questionIndex}.rules`,
          })}
          fieldPath={`${fieldPath}.questions.${questionIndex}`}
          values={values}
        />
      </Card>
    </div>
  ),
);
