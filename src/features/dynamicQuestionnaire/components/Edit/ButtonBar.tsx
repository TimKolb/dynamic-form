import * as React from 'react';
import { memo } from 'react';
import { Button, Popconfirm } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NamespacesConsumer } from 'react-i18next';

export const ButtonBar: React.FC<{ arrayHelpers: any; index: number }> = memo(
  ({ arrayHelpers, index }) => (
    <NamespacesConsumer>
      {t => (
        <Popconfirm
          title={t('Are you sure delete this question?')}
          onConfirm={() => arrayHelpers.remove(index)}
          okText={t('Yes')}
          cancelText={t('No')}
        >
          <Button type="ghost">
            <FontAwesomeIcon icon="trash" aria-label="Remove" />
          </Button>
        </Popconfirm>
      )}
    </NamespacesConsumer>
  ),
);
