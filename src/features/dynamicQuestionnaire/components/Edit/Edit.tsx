import * as React from 'react';
import { memo, ReactNode } from 'react';
import {
  ArrayHelpers,
  Field,
  FieldArray,
  FieldProps,
  Formik,
  FormikProps,
  getIn,
} from 'formik';
import { Button, Form, Input } from 'antd';
import * as Yup from 'yup';
import { AutoSave } from './AutoSave';
import { FieldType, FieldTypeType } from '../../fieldTypes';
import { GenericRuleType } from '../../validationRules';
import { QuestionnaireNameEdit } from './QuestionnaireNameEdit';
import { DragDropArrayField } from './DragDropArrayField';
import { NamespacesConsumer } from 'react-i18next';
import { getDefaultQuestion, getFormItemProps } from '../../formHelpers';
import { css } from 'emotion';
import {
  DragDropContext,
  DragDropContextProps,
  DraggableLocation,
} from 'react-beautiful-dnd';
import memoizeOne from 'memoize-one';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export type QuestionnaireSection = {
  name: string;
  questions: Question[];
};

export type Questionnaire = {
  name: string;
  sections: QuestionnaireSection[];
};

export type Question = {
  id: string;
  fieldType: FieldTypeType;
  question: string;
  rules: GenericRuleType[];
};

const questionnaireSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  sections: Yup.array().of(
    Yup.object().shape({
      name: Yup.string().required('A section should have a name'),
      questions: Yup.array()
        .of(
          Yup.object().shape({
            fieldType: Yup.string().required('Required'),
            question: Yup.string().required('Required'),
          }),
        )
        .min(1, 'A questionnaire should have min. question')
        .required('A questionnaire should have questions'),
    }),
  ),
});

const sectionNameInputClass = css`
  border: none;
  font-size: 1.5rem;
`;

export const Edit: React.FC<{
  questionnaire: Questionnaire;
  fieldTypes: FieldType[];
  onChange: (questionnaire: Questionnaire) => any;
  slotBefore?: ReactNode;
}> = memo(({ questionnaire, onChange, slotBefore }) => {
  return (
    <section>
      {slotBefore}
      <Formik
        validationSchema={questionnaireSchema}
        initialValues={questionnaire}
        onSubmit={v => alert(JSON.stringify(v, null, 2))}
        render={(formikProps: FormikProps<any>) => {
          const { values, errors, touched, handleSubmit } = formikProps;
          return (
            <form onSubmit={handleSubmit}>
              <QuestionnaireNameEdit errors={errors} touched={touched} />

              <hr />

              <FieldArray
                name="sections"
                render={arrayHelpers => (
                  <SectionEditor
                    arrayHelpers={arrayHelpers}
                    formikProps={formikProps}
                  />
                )}
              />

              <AutoSave values={values} onSave={onChange} debounce={0} />

              <Button htmlType="submit" type="primary">
                <NamespacesConsumer>{t => t('Submit')}</NamespacesConsumer>
              </Button>
            </form>
          );
        }}
      />
      <pre style={{ marginTop: '2rem' }}>
        <code>{JSON.stringify(questionnaire, null, 2)}</code>
      </pre>
    </section>
  );
});

const moveQuestionToOtherSection = (
  formikProps: FormikProps<any>,
  source: DraggableLocation,
  destination: DraggableLocation,
) => {
  const sourceList = [...getIn(formikProps.values, source.droppableId)];
  const destinationList = [
    ...getIn(formikProps.values, destination.droppableId),
  ];

  const [removed] = sourceList.splice(source.index, 1);
  destinationList.splice(destination.index, 0, removed);

  formikProps.setFieldValue(source.droppableId, sourceList);
  formikProps.setFieldValue(destination.droppableId, destinationList);
};

const reorder = (
  formikProps: FormikProps<any>,
  source: DraggableLocation,
  destination: DraggableLocation,
) => {
  const list = getIn(formikProps.values, source.droppableId);
  const reorderedList = [...list];
  const [removed] = reorderedList.splice(source.index, 1);
  reorderedList.splice(destination.index, 0, removed);
  formikProps.setFieldValue(destination.droppableId, reorderedList);
};

const createOnDragEnd = memoizeOne(
  (formikProps: FormikProps<any>) =>
    (({ source, destination }) => {
      if (!destination) {
        return;
      }

      if (source.droppableId === destination.droppableId) {
        reorder(formikProps, source, destination);
        return;
      }

      moveQuestionToOtherSection(formikProps, source, destination);
    }) as DragDropContextProps['onDragEnd'],
);

const SectionEditor: React.FC<{
  arrayHelpers: ArrayHelpers;
  formikProps: FormikProps<any>;
}> = memo(({ arrayHelpers, formikProps }) => (
  <div>
    <DragDropContext onDragEnd={createOnDragEnd(formikProps)}>
      {formikProps.values.sections.map(
        (section: QuestionnaireSection, index: number) => (
          <div key={index}>
            <FieldArray
              name={`sections.${index}.questions`}
              render={arrayHelpers => (
                <>
                  <Field
                    name={`sections.${index}.name`}
                    render={() => (
                      <Form.Item
                        {...getFormItemProps({
                          errors: formikProps.errors,
                          touched: formikProps.touched,
                          name: `sections.${index}.name`,
                        })}
                      >
                        <Field
                          name={`sections.${index}.name`}
                          render={({ field }: FieldProps) => (
                            <Input
                              {...field}
                              className={sectionNameInputClass}
                            />
                          )}
                        />
                      </Form.Item>
                    )}
                  />
                  <Button
                    onClick={() => arrayHelpers.push(getDefaultQuestion())}
                  >
                    <FontAwesomeIcon icon="plus" />
                  </Button>
                  <DragDropArrayField
                    formikProps={formikProps}
                    arrayHelpers={arrayHelpers}
                    values={section.questions}
                    sectionIndex={index}
                  />
                </>
              )}
            />
          </div>
        ),
      )}
    </DragDropContext>
  </div>
));
