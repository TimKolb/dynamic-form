import * as React from 'react';
import { memo } from 'react';
import { ArrayHelpers, FormikProps, FormikValues } from 'formik';
import { Droppable } from 'react-beautiful-dnd';
import { QuestionEdit } from './QuestionEdit';
import { fieldTypes } from '../../fieldTypes';
import { Question } from './Edit';
import memoizeOne from 'memoize-one';

const getListStyle = memoizeOne((isDraggingOver: boolean) => ({}));

export const DragDropArrayField: React.FC<{
  formikProps: FormikProps<any>;
  arrayHelpers: ArrayHelpers;
  values: FormikValues;
  sectionIndex: number;
}> = memo(({ formikProps, arrayHelpers, values, sectionIndex }) => (
  <Droppable droppableId={`sections.${sectionIndex}.questions`}>
    {(provided, snapshot) => (
      <div
        ref={provided.innerRef}
        style={getListStyle(snapshot.isDraggingOver)}
      >
        {(values as Question[]).map((question, index, all) => (
          <QuestionEdit
            key={question.id}
            question={question}
            questionIndex={index}
            all={all}
            arrayHelpers={arrayHelpers}
            formikProps={formikProps}
            fieldTypes={fieldTypes}
            fieldPath={`sections.${sectionIndex}`}
          />
        ))}
        {provided.placeholder}
      </div>
    )}
  </Droppable>
));
