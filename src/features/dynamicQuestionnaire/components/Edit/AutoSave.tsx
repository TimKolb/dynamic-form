import React, { PureComponent } from 'react';
import { debounce, isEqual } from 'lodash';

type AutoSaveState = { isSaving: boolean; lastSaved?: Date };
type AutoSaveProps<Values> = {
  debounce: number;
  onSave: (values: Values) => any;
  values: Values;
  render?: (state: AutoSaveState) => any;
  children?: (state: AutoSaveState) => any;
};

export class AutoSave<Values> extends PureComponent<
  AutoSaveProps<Values>,
  AutoSaveState
> {
  static defaultProps = {
    debounce: 300,
    render: null,
  };

  state = {
    isSaving: false,
  };

  componentDidUpdate(
    prevProps: AutoSaveProps<Values>,
    prevState: AutoSaveState,
  ) {
    if (!isEqual(prevProps.values, this.props.values)) {
      this.save();
    }
  }

  save = debounce(async () => {
    const { onSave, values } = this.props;
    this.setState({ isSaving: true });
    try {
      await onSave(values);
      this.setState({ isSaving: false, lastSaved: new Date() });
    } catch (e) {
      this.setState({ isSaving: false });
    }
  }, this.props.debounce);

  render() {
    const { render, children } = this.props;

    const fn =
      typeof children === 'function'
        ? children
        : typeof render === 'function'
        ? render
        : () => children || null;

    return fn(this.state);
  }
}
