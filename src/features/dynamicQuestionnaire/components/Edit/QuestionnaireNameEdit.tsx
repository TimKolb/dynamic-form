import * as React from 'react';
import { memo } from 'react';
import { Field, FieldProps, FormikErrors, FormikTouched } from 'formik';
import { Form, Input } from 'antd';
import { getFormItemProps } from '../../formHelpers';
import { NamespacesConsumer } from 'react-i18next';

const FormItem = Form.Item;

export const QuestionnaireNameEdit: React.FC<{
  errors: FormikErrors<any>;
  touched: FormikTouched<any>;
}> = memo(({ errors, touched }) => (
  <NamespacesConsumer>
    {t => (
      <FormItem
        label={t('Questionnaire')}
        {...getFormItemProps({
          errors,
          touched,
          name: 'name',
        })}
      >
        <Field
          name="name"
          render={({ field }: FieldProps) => <Input {...field} />}
        />
      </FormItem>
    )}
  </NamespacesConsumer>
));
