import * as React from 'react';
import { memo } from 'react';
import { Question } from '../Edit';
import { CustomFieldProps, FieldType, FieldTypeType } from '../../fieldTypes';
import { Field, FieldProps, FormikErrors, FormikTouched } from 'formik';
import { Alert, Form } from 'antd';
import { getFormItemProps } from '../../formHelpers';

const FormItem = Form.Item;

export type Values = { [id: string]: any };

function renderFieldForType<Values>(
  fieldProps: CustomFieldProps<Values>,
  fieldTypes: FieldType[],
  type: FieldTypeType,
) {
  const fieldType = fieldTypes.find((f: FieldType) => f.type === type);

  if (!fieldType) {
    return (
      <Alert
        message={`No field configuration found for field type '${type}'`}
        type="error"
      />
    );
  }

  const { Field } = fieldType;
  return <Field {...fieldProps} />;
}

export const QuestionViewItem: React.FC<{
  id: Question['id'];
  question: string;
  fieldType: FieldTypeType;
  fieldTypes: FieldType[];
  errors: FormikErrors<any>;
  touched: FormikTouched<any>;
  disabled: boolean;
}> = memo(
  ({ id, question, fieldType, errors, touched, disabled, fieldTypes }) => (
    <FormItem
      label={question}
      {...getFormItemProps({ errors, touched, name: id })}
    >
      <Field
        name={id}
        render={(formikFieldProps: FieldProps<Values>) =>
          renderFieldForType<Values>(
            {
              ...formikFieldProps,
              field: {
                ...formikFieldProps.field,
                disabled,
              },
            },
            fieldTypes,
            fieldType,
          )
        }
      />
    </FormItem>
  ),
);
