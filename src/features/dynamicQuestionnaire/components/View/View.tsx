import * as React from 'react';
import { memo, ReactNode } from 'react';
import { Button } from 'antd';
import { Formik, FormikProps } from 'formik';
import { buildValidationSchema } from '../../validationRules';
import { buildInitialValues } from '../../formHelpers';
import { FieldType } from '../../fieldTypes';
import { Questionnaire } from '../Edit';
import { QuestionViewItem, Values } from './QuestionViewItem';
import { NamespacesConsumer } from 'react-i18next';
import styled from '@emotion/styled';

const SectionHeading = styled.h2``;

export const View: React.FC<{
  questionnaire: Questionnaire;
  fieldTypes: FieldType[];
  disableAll: boolean;
  slotBefore?: ReactNode;
  onSubmit: (values: any) => void | Promise<void>;
}> = memo(({ questionnaire, fieldTypes, disableAll, slotBefore, onSubmit }) => (
  <section>
    {slotBefore}
    <Formik
      initialValues={buildInitialValues(questionnaire, fieldTypes)}
      validationSchema={buildValidationSchema(questionnaire, fieldTypes)}
      onSubmit={onSubmit}
      render={({ errors, touched, handleSubmit }: FormikProps<Values>) => (
        <form onSubmit={handleSubmit}>
          <h3>{questionnaire.name}</h3>
          {questionnaire.sections.map(
            section =>
              !!section.questions.length && (
                <section key={section.name}>
                  <SectionHeading>{section.name}</SectionHeading>
                  {section.questions.map(({ id, question, fieldType }) => (
                    <QuestionViewItem
                      key={id}
                      id={id}
                      question={question}
                      fieldType={fieldType}
                      errors={errors}
                      touched={touched}
                      fieldTypes={fieldTypes}
                      disabled={disableAll}
                    />
                  ))}
                </section>
              ),
          )}
          <Button htmlType="submit" disabled={disableAll}>
            <NamespacesConsumer>{t => t('Submit')}</NamespacesConsumer>
          </Button>
        </form>
      )}
    />
  </section>
));

View.defaultProps = {
  disableAll: false,
};
