import * as React from 'react';
import { Fragment, memo } from 'react';
import { Questionnaire } from './Edit';
import { FieldType } from '../fieldTypes';
import styled from '@emotion/styled';
import { Alert } from 'antd';

const QuestionnareTitle = styled.h3``;

export const Result: React.FC<{
  questionnaire: Questionnaire;
  fieldTypes: FieldType[];
  values: any;
}> = memo(
  ({ questionnaire, fieldTypes, values }) =>
    values && (
      <section>
        <QuestionnareTitle>{questionnaire.name}</QuestionnareTitle>
        {questionnaire.sections.map(section => (
          <section key={section.name}>
            {section.questions.map(q => {
              const fieldType = fieldTypes.find(f => f.type === q.fieldType);
              if (!fieldType) {
                return (
                  <Alert
                    key={q.id}
                    type="error"
                    message={`No fieldType found for ${q.fieldType}`}
                  />
                );
              }

              const fieldValue = values[q.id];

              const { Result } = fieldType;
              return (
                <Fragment key={q.id}>
                  <QuestionnareTitle>{q.question}</QuestionnareTitle>
                  <Result value={fieldValue} />
                </Fragment>
              );
            })}
          </section>
        ))}
      </section>
    ),
);
