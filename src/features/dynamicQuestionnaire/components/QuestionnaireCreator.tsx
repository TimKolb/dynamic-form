import * as React from 'react';
import { PureComponent } from 'react';
import styled from '@emotion/styled';
import { Switch } from 'antd';
import { NamespacesConsumer } from 'react-i18next';
import { ErrorBoundary } from '../../../common/components/ErrorBoundary';
import { Edit, Questionnaire } from './Edit';
import { fieldTypes, FieldTypeType } from '../fieldTypes';
import { GenericRuleType } from '../validationRules';
import { View } from './View';
import { Result } from './Result';

const ControlBar = styled('div')`
  flex: 1 0 100%;
  display: flex;
`;

const AppMain = styled('main')`
  display: flex;
  flex-wrap: wrap;
  > * {
    flex: 1;
    padding: 2rem 1rem;
  }
`;

const questionnaire: Questionnaire = {
  name: 'Einfache Fragen',
  sections: [
    {
      name: 'General',
      questions: [
        {
          id: Date.now().toString(),
          fieldType: FieldTypeType.shortText,
          question: 'Was geht so am Wochenende?',
          rules: [],
        },
        {
          id: Date.now().toString() + 1,
          fieldType: FieldTypeType.text,
          question: 'Was ist passiert?',
          rules: [],
        },
        {
          id: Date.now().toString() + 2,
          fieldType: FieldTypeType.rating,
          question: 'Wie war das letzte Wochenende?',
          rules: [GenericRuleType.required],
        },
      ],
    },
    {
      name: 'Anonymous',
      questions: [
        {
          id: Date.now().toString() + 3,
          fieldType: FieldTypeType.shortText,
          question: 'Verdienst du genug?',
          rules: [],
        },
        {
          id: Date.now().toString() + 4,
          fieldType: FieldTypeType.rating,
          question: 'Wie gerne arbeitest du?',
          rules: [GenericRuleType.required],
        },
      ],
    },
    {
      name: 'Red phone',
      questions: [
        {
          id: Date.now().toString() + 5,
          fieldType: FieldTypeType.text,
          question: 'Nachricht an die Geschäftsleitung',
          rules: [],
        },
      ],
    },
  ],
};

export class QuestionnaireCreator extends PureComponent<
  any,
  { questionnaire: Questionnaire; disableAll: boolean; values: any }
> {
  state = {
    questionnaire,
    disableAll: true,
    values: null,
  };

  handleQuestionnaireChange = (questionnaire: Questionnaire) => {
    this.setState({ questionnaire });
  };

  handleViewSubmit = (values: any) => {
    this.setState({ values });
  };

  toggleDisableAll = (enableAll: boolean) =>
    this.setState({ disableAll: !enableAll });

  render() {
    const { questionnaire, disableAll, values } = this.state;
    return (
      <NamespacesConsumer>
        {t => (
          <>
            <AppMain>
              <Edit
                fieldTypes={fieldTypes}
                questionnaire={questionnaire}
                onChange={this.handleQuestionnaireChange}
                slotBefore={<h2>{t('Edit')}</h2>}
              />
              <ErrorBoundary>
                <View
                  fieldTypes={fieldTypes}
                  questionnaire={questionnaire}
                  disableAll={disableAll}
                  onSubmit={this.handleViewSubmit}
                  slotBefore={
                    <>
                      <h2>{t('View')}</h2>
                      <ControlBar>
                        <div>
                          <Switch
                            checked={!disableAll}
                            onChange={this.toggleDisableAll}
                          />{' '}
                          {t('Enable fields')}
                        </div>
                      </ControlBar>
                    </>
                  }
                />
              </ErrorBoundary>
            </AppMain>
            <Result
              fieldTypes={fieldTypes}
              questionnaire={questionnaire}
              values={values}
            />
          </>
        )}
      </NamespacesConsumer>
    );
  }
}
