import * as React from 'react';
import * as Yup from 'yup';
import memoizeOne from 'memoize-one';
import { Schema } from 'yup';
import { FieldType } from './fieldTypes';
import { Questionnaire } from './components/Edit';

export enum GenericRuleType {
  required = 'required',
}

export type Rule<S = Schema<any>> = {
  type: string;
  label: string;
  add: (yupSchema: S) => S;
};

export const genericRuleTypes: Rule<any>[] = [
  {
    type: GenericRuleType.required,
    label: 'Required',
    add: yupSchema => yupSchema.required('Required'),
  },
];

export const buildValidationSchema = memoizeOne(
  (questionnaire: Questionnaire, fieldTypes: FieldType[]) => {
    const questionsSchema = questionnaire.sections.map(s => ({
      questions: Yup.array().of(
        Yup.object().shape(
          s.questions.reduce((mainYupRuleAcc, { id, rules, fieldType }) => {
            const type = fieldTypes.find(f => f.type === fieldType);
            if (!type) {
              throw new Error(`No field type found for ${fieldType}`);
            }

            const yupSchema = rules.reduce((yupSchema, ruleType) => {
              const rule = [...type.ruleTypes, ...genericRuleTypes].find(
                r => r.type === ruleType,
              );

              if (!rule) {
                throw new Error(`No rule found for ${ruleType}`);
              }

              return rule.add(yupSchema);
            }, type.yupType);

            return {
              ...mainYupRuleAcc,
              [id]: yupSchema,
            };
          }, {}),
        ),
      ),
    }));
    return Yup.object().shape(questionsSchema);
  },
);
