import * as React from 'react';
import './common/components/Icons';
import { Router } from '@reach/router';
import { LazyQuestionnairePage } from './features/dynamicQuestionnaire/pages/LazyQuestionnairePage';
import { Lazy404Page } from './common/pages/Lazy404Page';
import { LazyLoginPage } from './features/login/pages';

export const App = () => (
  <>
    <Router>
      <LazyQuestionnairePage path="/" />
      <LazyLoginPage path="/login" />
      <Lazy404Page default />
    </Router>
  </>
);
