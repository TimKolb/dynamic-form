import * as React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { ReactNode } from 'react';

type LanguageIcons = { [key: string]: ReactNode };
const languageIcons: LanguageIcons = {
  en: (
    <span role="img" aria-label="English">
      🇬🇧
    </span>
  ),
  de: (
    <span role="img" aria-label="Deutsch">
      🇩🇪
    </span>
  ),
};

type ChangeLanguageCallbackValues = {
  languageIcons: LanguageIcons;
  language: string;
  onChangeLanguage: (lang: string) => Promise<void>;
};

const ChangeLanguageComp: React.FC<
  WithNamespaces & {
    children: (changeLanguageCBValues: ChangeLanguageCallbackValues) => any;
  }
> = ({ t, i18n, children }) => {
  const handleChangeLanguage = async (lang: string) => {
    await i18n.changeLanguage(lang);
  };

  const { language } = i18n;

  return children({
    languageIcons,
    language,
    onChangeLanguage: handleChangeLanguage,
  });
};

export const ChangeLanguage = withNamespaces()(ChangeLanguageComp);
